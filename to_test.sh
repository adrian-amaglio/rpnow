#!/bin/bash


if [ "$1" = "prod" ] ; then
  tag=rpnom
  name=rpnow.jean-cloud.org
  port=3236
  volume=/home/docker/rpnow.jean-cloud.org:/var/local/rpnow
elif [ "$1" = 'test' ] ; then
  tag=rpnom:dev
  name=test.rpnow.jean-cloud.org
  port=3237
  volume=/home/docker/test.rpnow.jean-cloud.org:/var/local/rpnow
else
  echo "Must have arg 'prod' or 'test'"
  exit 1
fi

git pull

docker build -t "$tag" .
e="$?"
if [ "$e" -ne 0 ] ; then
  echo "Build exited with error code '$e'"
  exit 1
fi

docker stop "$name" && docker rm "$name"
docker run -p "$port":80 -d --name "$name" -v "$volume" rpnow

